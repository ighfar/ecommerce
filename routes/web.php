<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'BerandaController@index');

Route::prefix('admin')->middleware(['auth','oauth:admin'])->group(function(){
	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('media','HomeController@media')->name('media.index');
	Route::get('dashboard','HomeController@index');
	Route::resource('category','CategoryController');
	Route::resource('product','ProductController');
	//transaction
	Route::get('transaction','TransactionController@index')->name('transaction.index');
	Route::get('transaction/{code}/{status}','TransactionController@status');
	Route::get('transaction/{code}/detail/data','TransactionController@detail');
	Route::get('transaction/{code}/detail/data/cetak','TransactionController@cetakpdf');
	//users
	Route::get('user','UserController@index')->name('admin.user');
	Route::get('user/status/{id}','UserController@changestatus');
	Route::get('user/add','UserController@create')->name('admin.user.create');
	Route::post('user/add','UserController@store')->name('admin.user.store');
	Route::get('user/edit/{id}','UserController@edit');
	Route::post('user/update','UserController@update');
	Route::get('user/delete/{id}','UserController@delete');

});
